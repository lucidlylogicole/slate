let version = '2.0.0'

function Terminal(options) {
    
//---o:
let Term = {
    
//---Commands
    setCommand:function(cmd,fnc,options) {
        if (options === undefined){options = {}}
        if (options.app === undefined) {options.app = 'main'}
        else if (!(options.app in Term.apps)) {
            Term.addApp(options.app,{cmd:fnc})
        }
        Term.apps[options.app].commands[cmd] = fnc
        if (options.help){Term.apps[options.app].help[cmd] = options.help}
    },
    
    setCommands:function(cmdDict,app) {
        if (app === undefined) {app = 'main'}
        else if (!(app in Term.apps)) {
            Term.addApp(app)
        }
        for (let cmd in cmdDict) {
            Term.setCommand(cmd,cmdDict[cmd],{app:app})
        }
    },
    
    setHelp:function(cmd,description,app) {
        if (app === undefined) {app = 'main'}
        Term.apps[app].help[cmd] = description
    },
    
//---Input Commands
    terminalKeyDown:function(event) {
        // console.log(event.keyCode)
        let handled = 0
        if (event.shiftKey === true && event.keyCode == 13) {
            // Shift Enter
            Term.elements.input.rows=Term.elements.input.rows+1
        } else if (event.keyCode == 13) {
            // Enter
            handled= 1
            if (Term.wait_for_input) {
                Term.wait_for_input = 0
                // Clear wait for input and allow that to handle input
            } else {
                Term.inputCommand(Term.elements.input.value)
                Term.clearInput()
            }
        } else if (event.keyCode == 33) {
            // Page Up
            Term.elements.container.scrollTop -= Term.elements.container.offsetHeight*0.8
            handled = 1
        } else if (event.keyCode == 34) {
            // Page Down
            Term.elements.container.scrollTop += Term.elements.container.offsetHeight*0.8
            handled = 1
        } else if (Term.track_history) {
            if (event.keyCode == 38) {
                // Up Arrow
                Term.inputHistory(-1)
                handled = 1
            } else if (event.keyCode == 40) {
                // Down Arrow
                Term.inputHistory(1)
                handled = 1
            }
        }

        // Prevent Key press
        if (handled) {event.preventDefault()}
    },
    
    clearInput:function() {
        Term.elements.input.value = ''
        Term.elements.input.rows = 1
    },
    
    setInputText:function(txt) {
        Term.elements.input.value = txt
        Term.elements.input.selectionStart = Term.elements.input.selectionEnd = Term.elements.input.value.length;
    },
    
    getInput:function(text) {
        Term.wait_for_input = 1
        let term = this
        let cur_prompt = Term.cursor_prefix
        if (text) {
            text += ' '
            Term.setPrompt(text)
        }
        return new Promise((resolve,reject)=>{
            function check() {
                if (Term.wait_for_input) {
                    setTimeout(check,100)
                } else {
                    Term.wait_for_input = 0
                    let v = Term.elements.input.value
                    if (Term.echo_commands) {
                        Term.output('<div class="cmd">'+Term.cursor_prefix+v+"</div>")
                    }
                    resolve(v)
                    Term.clearInput()
                    if (text) {Term.setPrompt(cur_prompt)}
                }
            }
            setTimeout(check,100)
        })
    },
    
    inputCommand:function(cmd_txt) {
        if (Term.echo_commands) {
            Term.output('<div class="cmd">'+Term.cursor_prefix+cmd_txt+"</div>")
        } else {
            Term.elements.output.innerHTML += '\n'
        }
        
        // Store History of Command
        if (Term.track_history) {
            Term.storeHistory(cmd_txt)
        }
        
        // Split for command
        let cmd, cmd_args
        let l_cmd_txt = cmd_txt.toLowerCase()
        let cmd_match = cmd_txt.match(/^(\S+)\s(.*)/)
        if (cmd_match !== null) {
            let cmd_split = cmd_match.slice(1)
            cmd = cmd_split[0].toLowerCase()
            cmd_args = cmd_split[1]
        }
        
        // Check for full text command in Current App
        if (l_cmd_txt in Term.apps[Term.current_app].commands) {
            let out = Term.apps[Term.current_app].commands[l_cmd_txt]()
            if (out !== undefined && !(out instanceof Promise)) {
                Term.output(out)
            }
        // Check for first part of command in Current app
        } else if (cmd in Term.apps[Term.current_app].commands) {
            let out = Term.apps[Term.current_app].commands[cmd](cmd_args)
            if (out !== undefined && typeof(out)!= 'object') {
                Term.output(out)
            }
        // If apps enabled, check if full command in app
        } else if (Term.app_cmd_opens_app && (l_cmd_txt in Term.apps)) {
            // Check if command is the name of an app (if so open)
            Term.setCurrentApp(l_cmd_txt)
        } else if (Term.current_app != 'main') {
            // Check if full command is in main application
            if (l_cmd_txt in Term.apps.main.commands) {
                let out = Term.apps.main.commands[l_cmd_txt]()
                if (out !== undefined) {
                    Term.output(out)
                }
            // Check if start of command in current application
            } else if (cmd in Term.apps[Term.current_app].commands) {
                let out = Term.apps[Term.current_app].commands[cmd](cmd_args)
                if (out !== undefined) {
                    Term.output(out)
                }
            } else {
                Term.apps[Term.current_app].else(cmd_txt)
            }
        } else if (Term.eval_enabled){
            // Run JavaScript command if enabled
            try {
                if (Term.eval_mode == 'global') {
                    Term.output(window.eval(cmd_txt))    // Using Global scope
                } else if (Term.eval_mode == 'local') {
                    Term.output(eval(cmd_txt))       // Using local scope
                }
            } catch(e) {
                Term.output('<i>undefined or error</i>')
            }
        } else {
            Term.else(cmd_txt)
        }
    },
    
    storeHistory:function(cmd_txt) {
        // Store History
        let ok = 1
        if (Term.command_history.length >0) {
            // Ignore history if command is the same
            if (Term.command_history[Term.command_history.length -1] == cmd_txt) {
                ok = 0
            }
        }
        if (ok) {
            Term.command_history.push(cmd_txt)
        }
        
        // Add History Count
        Term.history_position = Term.command_history.length
    },

    //---f:
    inputHistory:function(inc) {
        // Get input command history (1 for above, -1 for previous)
        if (Term.command_history.length > 0) {
            
            Term.history_position += inc
            let end_flg = 0
            
            if (Term.history_position >= Term.command_history.length) {
                Term.history_position -= 1
                Term.setInputText('')
                end_flg = 2
            } else if (Term.history_position < 0) {
                Term.history_position = 0
            }
            
            if (!end_flg) {
                Term.setInputText(Term.command_history[Term.history_position])
            }
        }
    },
    
    setPrompt:function(txt) {
        Term.cursor_prefix = txt
        Term.elements.prompt.innerHTML = txt.replace(/ /gi,'&nbsp;')
        Term.focus()
    },
    
    setInputVisible:function(visible) {
        if (visible) {
            Term.elements.input_container.style.display = 'flex'
            Term.focus()
        } else {
            Term.elements.input_container.style.display = 'none'
        }
    },

    setInputFixed:function(fixed) {
        if (fixed) {
            Term.elements.container.classList.add('fixed')
        } else {
            Term.elements.container.classList.remove('fixed')
        }
    },

    focus: function() {
        Term.elements.input.focus()
    },

//---Output
    output:function(out_txt,auto_scroll) {
        Term.elements.output.innerHTML += out_txt
        
        // Autoscroll output element (default is true)
        if (auto_scroll === undefined) {auto_scroll = 1}
        if (auto_scroll) {
            Term.elements.output.scrollTo(0,Term.elements.output.scrollHeight)
        }
        
        Term.outputChanged() // Output Event
    },

    outputChanged:function() {
        let evt = new CustomEvent('outputChanged', {});
        Term.elements.container.dispatchEvent(evt);
    },

    clearOutput:function() {
        // Term.output('')
        Term.elements.output.innerHTML = ''
    },

    // Print current app command list
    printCommandList:function(app) {
        if (app === undefined) {app = Term.current_app}
        Term.output(' Commands')
        
        // Find max length
        let max_length = 4
        for (let ky in Term.apps[app].commands) {
            if (ky.length > max_length) {max_length = ky.length}
        }
        for (let ky in Term.apps) {
            if (ky.length > max_length) {max_length = ky.length}
        }
        
        // Show command list
        for (let ky in Term.apps[app].commands) {
            let htxt = '\n    '+ky.padEnd(max_length+3)
            if (ky in Term.apps[app].help) {
                htxt += Term.apps[app].help[ky]
            }
            Term.output(htxt)
        }
        
        // Show Apps if main
        if (app == 'main') {
            Term.output('\n Apps')
            for (let ky in Term.apps) {
                if (ky != 'main') {
                    let desc = '' || Term.apps[ky].description
                    Term.output('\n    '+ky.padEnd(max_length+3)+desc)
                }
            }
        
        }
    },

//---Apps
    setCurrentApp:function(app_name) {
        let ok = 1
        if (Term.current_app !== 'main') {
            ok = Term.apps[Term.current_app].onexit()
        }
        if (ok || ok ===undefined) {
            Term.current_app = app_name
            Term.currentAppChanged(app_name) 
            Term.apps[app_name].onload()
        }
    },
    
    currentAppChanged:function(app_name) {
        let evt = new CustomEvent('currentAppChanged', {detail: app_name})
        Term.elements.container.dispatchEvent(evt)
    },
    
    addApp:function(name,commandD,description) {
        
        let appD = {
            name:name,
            settings:{},
            commands:{},
            description:description,
            help:{},
            onload:function(){},     // onload function
            onexit:function(){},     // onload function
            appChanged:function(){},
            else:function(){},       // else no commands found
            setCommand: function(cmd,fnc,options) {
                appD.commands[cmd] = fnc
                if (options && options.help){appD.help[cmd]=options.help}
            },
        }
        if (commandD != undefined) {
            appD.commands = commandD
        }
        
        Term.apps[name] = appD
        return appD
    },
    
    removeApp:function(app_name) {
        if (! app_name == 'main') {
            if (Term.current_app == app_name) {
                Term.current_app = 'main'
            }
            // Remove app if not in main
            delete Term.apps[app_name]
        }
    },
}

//---
//---Options
if (options === undefined) {options = {}}

// Default Options
Term.current_app = 'main'        // Stores the current app
Term.cursor_prefix = '❭ '

// Setup Main App
Term.apps = {}
Term.addApp('main')
Term.setCommand('clear',Term.clearOutput,{help:'clear the screen'})
Term.setCommand('help',Term.printCommandList,{help:'view commands and apps'})
Term.setCommand('exit',function(){Term.setCurrentApp('main')},{help:'exit the current app'})

// Settings
Term.echo_commands = 1      // write input commands on screen
Term.app_cmd_opens_app = 1  // Open the app (set current app) if the app is input as a command
Term.wait_for_input = 0     // Check when using get_input

// Enable JavaScript eval (runs if commands not found)
Term.eval_enabled = 0
Term.eval_mode = 'global'   // global or local scope for eval

// Keep track of command history
Term.track_history = 1
Term.command_history = []
Term.history_position = 0

Term.else = function(){}    // else function

// Set Options
for (let opt in options) {
    Term[opt] = options[opt]
}

//---Build
// Container And Output Display
Term.elements = {}
Term.elements.container = document.createElement('div')
Term.elements.container.className = 'terminal-box'
Term.elements.output = document.createElement('pre')
Term.elements.output.className = 'terminal-output'
Term.elements.container.appendChild(Term.elements.output)

// Input Container
Term.elements.input_container = document.createElement('div')
Term.elements.input_container.className = 'terminal-input'
// Input Prompt
Term.elements.prompt = document.createElement('span')
Term.elements.input_container.appendChild(Term.elements.prompt)
// Input Text Area
Term.elements.input = document.createElement('textarea')
Term.elements.input.rows = 1
Term.elements.input_container.appendChild(Term.elements.input)

Term.elements.container.appendChild(Term.elements.input_container)

// Add to main
if (options.parent !== undefined) {
    if (typeof(options.parent) == 'string'){
        options.parent = document.getElementById(options.parent)
    }
    options.parent.appendChild(Term.elements.container)
}

//---Events
Term.elements.input.onkeydown = Term.terminalKeyDown

Term.elements.container.addEventListener('outputChanged', function (e) {
    // Scroll to bottom of pages
    Term.elements.container.scrollTop = Term.elements.container.scrollHeight
    // window.scrollTo(0,Term.elements.container.scrollHeight)
})

Term.elements.container.onclick = function() {
    if (document.getSelection().isCollapsed) {
        Term.focus()
    }
}

Term.setPrompt(Term.cursor_prefix)

return Term

}
# Slate Terminal
A simple terminal interface to embed on a webpage.  It handles the basics of the interface while you control what it does.

## Features
- simple to implement with basic input and output text functions
- style your own way
- command history with up/down
- optional JavaScript eval mode (disabled by default)

<img src="https://gitlab.com/lucidlylogicole/slate/raw/demos/slate_screenshot.png">

## Requirements
- A modern HTML5 browser
- No additional modules like jquery are needed

## License
- [MIT](license)

## Installation
1. Download the [slate-terminal.js](https://gitlab.com/lucidlylogicole/slate/raw/master/slate-terminal.js) file and [slate-terminal.csss](https://gitlab.com/lucidlylogicole/slate/raw/master/slate-terminal.css)
2. Reference it on your html page

## [Demo](https://lucidlylogicole.gitlab.io/slate/terminal.html)
- A basic demo with option toggles

## Quickstart
1. Include the slate-terminal.js file

        <script src="slate-terminal.js"></script>
        <link href="slate-terminal.css" rel="stylesheet" media="screen">

2. Have a containing element to add the terminal to

        <div id="terminal-box"></div>

3. Create the terminal and pass the parent element

        <script>
            term = Terminal({parent:'terminal-box'})
            
            // Add a custom command to the terminal
            term.setCommand('hi', function() {return 'hello!'})
            
        </script>

4. Optionally customize the style of the terminal by styling the containing element.  Here is a black background and lightblue/white theme:

        <style>
            #terminal-box {
                height:400px;
                border-radius:3px;
                background:rgb(30,30,33);
                color:white;
            }
            .cmd, .terminal-input {
                color:lightblue;
            }
        </style>

----

# Documentation

## Terminal(options)
The main terminal object
- **options** (dict) the options for the terminal

        let term = Terminal(options)

## options
The options can be set during or after creating a new terminal object

        let term = new Terminal({parent:'terminal-box',
            echo_commands:0
        })
        
or

        let term = new Terminal({parent:'terminal-box'})
        term.echo_commands = 0

- **parent** (string or element) - the element to insert the terminal into
- **echo_commands=1** - write input commands on screen (default is 1/true)
- **eval_enabled=0** - enable the terminal to parse the command as JavaScript if no other specified commands are found (default is 0/false)
- **eval_mode='global'** - use global or local scope for the JavaScript eval (default is 'global')
- **track_history=0** - keep track of input command history (default is 0/false)

## Commands
The Slate Terminal works by processing a list of commands you specified.  Those commands are stored in Terminal.commandD. *Commands must be lowercase* as the user input is converted to lowercase before parsing.

- the command function can return a string that will then automatically be output on the terminal
- the command function can also handle an arguments which is any text the user types (after the space) after command text
    - the rest of the text after command is returned as a single string.  It is up to you to process that for your command.
    - the argument string can be case sensitive
- commands can be tied to multiple apps or to the 'main' app

## Add / Set a Command
**term.setCommand('mycommand', myfunction, options)**

When a user types *mycommand* in the terminal, it will execute *myfunction* (after they press return).

__Example 1:__

        Terminal.setCommand('hi', function(){return 'hello'}
        
- When a users types *hi* then *hello* will be output on the screen

        ❭ hi
        hello


__Example 2:__

        Terminal.setCommand('hi', function(txt){return 'hello '+txt}

- Any additional text after the space after the command will be returned to the function
- When a user types *hi Joe* then the string *Joe* will be passed to the function and the output will be *hello Joe*

        ❭ hi Joe
        hello Joe


### setCommand Options


## Terminal Functions
Some functions of the Terminal object that you may want to use

- **.clearInput()** - clears the input box
- **.setInputText(txt)** - sets the input text but does not execute
- **.inputCommand(cmd_txt)** - executes the command
- **.output(txt, auto_scroll)** - adds text to the output screen (pre)
    - *auto_scroll* - terminal automatically scrolls the output element.  set this to 0 to avoid scrolling
- **.clearOutput()** - clears the output screen
- **.setPrompt(txt)** - set what shows up before the cursor
- **.setInputVisible(visible)** - show and hide the input field
- **.focus()** - set cursor focus on the terminal input
- **.setCommand('mycommand', myfunction, options)** - sets the command text *mycommand* to the function *myfunction*
- **.setCommands(commandDict)** - sets all commands from the *commandDict* dictionary to the terminal
- **.getInput(text)** - pause execution of function and get the user input (your function needs to be async and precede this with await)
- **setHelp(cmd,description)** - set the help description for the command

## Input Key Functions
The Slate Terminal input handles the following key functions:
- **Enter** - executes the command by calling *Terminal.inputCommand*
- **Shift+Enter** - adds a new line on the input
- **Up Arrow** - goes to the previous entered command (if command history is enabled)
- **Down Arrow** - scrolls down one through the command history (if command history is enabled)
- **Page Up** - scrolls the output up by 80% of the height
- **Page Down** - scrolls the output down by 80% of the height

## Apps
The Slate Terminal has one app named *main* that is the default app.  Any commands  are tied to the main app by default.  For more complicated setups, you can have apps that have their own separate command sets.

- app commands are only evaluated for the current app
- apps must be defined as lowercase
- to set a command to a specific app, there is an optional app argument
    - **.setCommand('mycommand', myfunction, {app:appname})**
    - **.setCommands(commandDict, appname)**
- if a command is equal to an app name, the terminal will set that as the current app
    - you can disable this by setting
        - term.app_cmd_opens_app = 0
- **.setCurrentApp(app_name)** - to set the current app use
- **.addApp(app)** - add an app with an app object

        let myapp = new App('myapp')
        term.addApp(myapp)


- **.removeApp(app_name)** - remove an app

### App Object
Slate Terminal will automatically create new apps with the setCommand function.  For more control you can create your own app object

- **addApp(app_name, commandDict, help)** - the app object is returned
    - *app_name* (str) - the name of the app
    - *commandDict* - (optional) a dictionary with a list of commands and associated functions
    - *help* (str) - help description for the app
    - **.commands** - a dictionary containing the list of commands
    - **.onload()** - a function you can override which will be run every time the app is loaded
    - **else(cmd_txt)** - optional function that can be provided that is called if no other commands are triggered for the app
    

        // Create App (with print function)
        myapp = new App('myapp', {'print':function(txt) {return txt}})
        myapp.onload = function(){'do something when loading this app'}
        myapp.commands['hi'] = function(){return 'hello'}  // add a command called hi

## Events
- **outputChanged** - event is triggered when the output function is called
    - to use this event to scroll the window:
        
            term.elements.container.addEventListener('outputChanged', function (e) {
                window.scrollTo(0,document.body.scrollHeight);
            })

- **currentAppChanged** - Event triggered when the current app is changed.
    - to listen to this event (on the terminal container):
    
            term.elements.container.addEventListener('currentAppChanged', function (e) {
                txt = ''
                if (e.detail != 'main') {txt = e.detail}
                document.getElementById('prompt').innerHTML = txt +' '+ '&rsaquo;'
            })

## Wait for User Input
The Slate Terminal has a way to block execution, await a response, and get that response through the **getInput** function.  The getInput is a promise, so you have to use async/await to block in your function.  The following example has a command *temp* that will ask for a temperature, wait, calculate and then return a result.

    term.setCommand('temp', async function(){
        let t = await term.getInput('Enter the temperature in Celsius')
        term.output(t +'C = '+Math.round(ti*9/5+32)+'F')
    })

## Default Commands
The following commands are included by default:

- **clear** - clears the output screen
- **exit** - will exit the current app (set the current app to main)
- **help** - lists the available commands and apps

To overwrite them:

    term.setCommand('clear',()=>{})
    // or 
    delete term.apps.main.commands['clear']